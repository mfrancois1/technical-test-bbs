import React, { FC, Fragment } from 'react';
import PlanetItem from './../PlanetItem/index';
import SearchBar from './../SearchBar/SearchBar';
import classes from './Planets.module.css';

interface DataProps {
    data: {
        name: string,
        discoveredBy: string,
        discoveryDate: string,
        avgTemp: number,
        image: string,
    }[],
    getTextSearchBar: (arg: string | undefined) => void,
}

const Planets: FC<DataProps> = props => {
    const textSearchBar = (data: string | undefined):void => {
        props.getTextSearchBar(data);
    }
    
    return (
        <section className={classes.section}>
            <h2>Affichez une planète précise grâce au champ recherche</h2>
            <SearchBar textSearchBar={textSearchBar}/>
            <ul className={classes.ul}>
                {
                    props.data.map((item, index) => {
                        return <PlanetItem
                            name={item.name}
                            discoveredBy={item.discoveredBy}
                            discoveryDate={item.discoveryDate}
                            avgTemp={item.avgTemp}
                            image={item.image}
                            key={index}
                        />
                    })
                }
            </ul>
        </section>
    );
};

export default Planets;