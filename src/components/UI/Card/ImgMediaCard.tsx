import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import PLanetPage from './../../../pages/Planet/Planet';

interface DataProps {
    name: string,
    discoveredBy: string,
    discoveryDate: string,
    avgTemp: number,
    image: string,
}

const useStyles = makeStyles({
    root: {
        width: 300,
    },
});

export default function ImgMediaCard(props: DataProps) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt={`Photo de la planète ${props.name}`}
                    height="140"
                    image={props.image}
                    title={`Photo de la planète ${props.name}`}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.name}
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button size="small" color="primary">
                    <Link to={{
                        pathname: `/planet/${props.name.toLowerCase()}`,
                        state: {
                            name: props.name,
                            discoveredBy: props.discoveredBy,
                            discoveryDate: props.discoveryDate,
                            avgTemp: props.avgTemp,
                            image: props.image,
                        }
                    }}>
                        En savoir plus
                    </Link>
                </Button>
            </CardActions>
        </Card>
    );
}