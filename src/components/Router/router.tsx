import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from './../../App';
import PlanetPage from './../../pages/Planet/Planet';

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" component={App} exact/>
                <Route path="/planet/:id" component={PlanetPage}/>
            </Switch>
        </BrowserRouter>
    );
};

export default Router;