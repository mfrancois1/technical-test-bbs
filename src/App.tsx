import React, { useState, useEffect, FC } from 'react'
import { Paper } from '@material-ui/core'
import CssBaseline from '@material-ui/core/CssBaseline'
import { StylesProvider, ThemeProvider } from '@material-ui/core/styles'
import Header from './layouts/Header/Header';
import Planets from './components/Planets/index';
import PlanetPage from './pages/Planet/Planet';
import { app } from './configuration'

type IPlanet = {
    name: string,
    discoveredBy: string,
    discoveryDate: string,
    avgTemp: number,
    image: string,
}

const App: FC = (props) => {
    const {children} = props

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [data, setData] = useState<IPlanet[]>([]);
    const [filteredData, setFilteredData] = useState<IPlanet[]>([]);
    const [inputValue, setInputValue] = useState<string | undefined>('');

    const planetsFiltered = ['Mercure', 'Vénus', 'Mars', 'Jupiter', 'Saturne', 'Uranus', 'Neptune'];
    
    useEffect(() => {
        fetch("https://api.le-systeme-solaire.net/rest/bodies/")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);

                    const dataFiltered = result.bodies.filter((item: { name: string; }) => {
                        return planetsFiltered.includes(item.name)
                    });

                    addImages(dataFiltered);
                    setData(dataFiltered);

                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    const addImages = (data: { 
        name: string,
        discoveredBy: string,
        discoveryDate: string,
        avgTemp: number,
        image: string,
    }[]): void => {
        data.map(item => {
            if(item.name === 'Mercure') {
                item.image = "https://cdn.futura-sciences.com/sources/images/True_Mercury.jpg";
            } else if (item.name === 'Vénus') {
                item.image = "https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/b/1/4/b14c8f7a8f_119688_venus-uv1-akatskuki-damiabouic.jpg";
            } else if (item.name === 'Mars') {
                item.image = "https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/d/1/5/d15638aac8_50158183_mars-glace-carte-canonqie.jpg";
            } else if (item.name === 'Jupiter') {
                item.image = "https://cdn.futura-sciences.com/sources/images/Jupiter_Adobe_Stock.jpg";
            } else if (item.name === 'Saturne') {
                item.image = "https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/1/8/0/180c307018_50012326_sat-02.jpg";
            } else if (item.name === 'Uranus') {
                item.image = "https://cdn.futura-sciences.com/cdn-cgi/image/width=1024,quality=50,format=auto/sources/images/Uranus-Voyager2.jpg";
            } else if (item.name === 'Neptune') {
                item.image = "https://cdn.futura-sciences.com/sources/images/Neptune_voyager_2_nasa.jpg";
            }
        })
    }

    useEffect(() => {
        if (inputValue) {
            const regex = RegExp(inputValue, 'i');
            const filterWithRegex = data.filter((planet) => regex.test(planet.name));
            setFilteredData(filterWithRegex);
        }
    }, [inputValue])

    const getTextSearchBar = (data: string | undefined): void => {
        setInputValue(data);
    }

    return (
        <React.Fragment>
            <Header/>
            { 
                inputValue ? 
                    <Planets getTextSearchBar={getTextSearchBar} data={filteredData}/> 
                    : 
                    <Planets getTextSearchBar={getTextSearchBar} data={data} /> 
            }
        </React.Fragment>
    )
}
export default App
