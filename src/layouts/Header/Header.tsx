import React, {FC, useState} from 'react';
import SearchBar from '../../components/SearchBar/SearchBar';

const Header = () => {
    return (
        <header>
            <h1>Découvrez les planètes de notre système solaire</h1>
        </header>
    );
};

export default Header;