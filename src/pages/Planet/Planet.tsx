import React, { useState, useEffect, Fragment } from 'react';
import { useLocation, Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import classes from './Planet.module.css';

type planetPageProps = {
    name: string,
    discoveredBy: string,
    discoveryDate: string,
    avgTemp: number,
    image: string,
}

const PlanetPage = (props: planetPageProps) => {
    const location = useLocation();
    const state = location.state as planetPageProps;

    return (
        <Fragment>
            <header>
                <h1>la planète {state.name}</h1>
            </header>
            <section className={classes.header}>
                <div>
                    <Button variant="outlined" size="small" color="primary">
                        <Link to="/">
                            Retour
                        </Link>
                    </Button>
                </div>
                <div className={classes.containerInfoPlanet}>
                    <div>
                        <img src={state.image} alt={`Une photo de la planète ${state.name}`}/>
                    </div>
                    <div>
                        <h2>Auteur(s) de la découverte :</h2>
                        {state.discoveredBy ? <p>{state.discoveredBy}</p> : <p>Aucune information</p>}
                        <h2>Date de la découverte :</h2>
                        {state.discoveryDate ? <p>{state.discoveryDate}</p> : <p>Aucune information</p>}
                        <h2>Température moyenne sur la planète :</h2>
                        {state.avgTemp ? <p>{state.avgTemp} Kelvin</p> : <p>Aucune information</p>}
                    </div>
                </div>
            </section>
        </Fragment>
    );
};

export default PlanetPage;