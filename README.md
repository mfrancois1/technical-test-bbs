# Boilerplate React JS / App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and uses [Material UI](https://material-ui.com/)

## Installation

### Copy .env file :

```
cp .env.example .env
```

### Set node version

```
nvm use
```

### Install dependencies

```
npm insall
```

## Run project

### Start

`npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Build

`npm run build`

Builds the app for production to the `build` folder.
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### Storybook

`npm run storybook`


# Exercice
En se basant sur l'api https://api.le-systeme-solaire.net : 
1. Afficher la liste des planettes du système solaire (image, nom).
2. Avoir la possibilité de filtrer par un champs text sur le nom de la planette.
3. Au clique afficher une page détail (avec une route) pour afficher les détailles de la planette. Libre à vous d'afficher ce que vous voulez.

En option utiliser les composant material UI qui est déjà inclut dans le projet.

# Retour sur l'exercice

## Les consignes

J'ai pu respecter les 3 principales consignes de l'exercice :

### 1. Afficher la liste des planètes
Après avoir récupéré les données de tous les astres via une requête vers l'API, trié uniquement les planètes de 
notre système solaire, et ajouté des images pour chaque objet, je les ai affiché dans un composant card de materiel UI.

### 2. Filtrer par le nom
Un filtrage est réalisé dès que la valeur du champ est modifiée. On vient tester si les noms des planètes contiennent
les caractères contenus dans le champ.

### 3. Afficher la page dédiée d'une planète
Au clique, on affiche la page dédiée d'une planète avec ses caractéristique grâce à react-router. 
On récupère de manière dynamique les données via le state et modifie l'URL en fonction de la planète.

### En options
J'ai utilisé les composants Button et Card de material UI par contre je n'ai pas eu le temps de créer les composants 
dans storybook.

## Les problèmes rencontrés
Je voulais, au début, utiliser la structure déjà présente dans le projet, mais je n'ai pas réussi à comprendre tout
le fonctionnement. J'ai donc fait un tri des imports et composants pour appliquer ce que j'avais appris lors des mes
précédents projets.
Je n'avais que de très petites notions en TypeScript et malheureusement, j'ai perdu beaucoup de temps et 
de cheveux à déboguer les erreurs TypeScript ! Le système de typage demande de l'investissement et 
de la rigueur quand on a pas l'habitude.
C'était la 1ère fois que j'utilisais la librairie React-router. 
En terme de style, je pense que le résultat est correct mais j'aurais souhaité avoir plus de temps pour rajouter
des animations et effets de transition.

## Axes d'amélioration
Je dois suivre un tutoriel sur les bases de TypeScript sans le coupler à React pour commencer.
J'aimerais apprendre à utiliser Redux pour éviter de passer les props de composant en composant.

## Mes impressions
J'ai vraiment apprécié travailler sur ce projet. J'avais justement prévu de réaliser un exercice similaire pour revoir
les bases de React et apprendre de nouvelles librairies comme react-router ou redux.
Je me rends compte que j'ai encore beaucoup de travail à fournir et que ma marge d'évolution est importante.  
Néanmoins, j'ai appris des choses et ce test m'a encouragé dans mon choix de sortir de ma zone de confort et me diriger
vers un framework Javascript.

Vous pouvez retrouver mes deux précédents projets en React :
* [Pour apprendre les bases](https://gitlab.com/Boris74000/learn-react-food-order-app)
* [Projet soutenu lors de mon parcours de développeur front-end avec OpenClassrooms](https://gitlab.com/Boris74000/lancez_votre_propre_site_d_avis_de_restaurants)